# Bugs connus

* Quand on arrête le `server`, en suite Le `client` envoie des données sur la `socket`. Le `client` crache subitement sans caché aucune erreurs. 
Le code de sortie est 141 qui correspond au signal système `SIGPIPE`.                    
```bash
$ echo $?
141
```
# Evolutions
* Persistances des évènements et des clients inscrits en base de données; un dossier `src/db/` est prévu pour
  * Clients dans `src/db/clients.db` | 1 Jour.
  * Evènements dans `src/db/events.db` (fonctionnalité déjà implémentée).
* Optimisation de la bufférisassions (`fread, fwrite` calculer précisément le nombre d'octets à lire et/ou à écrire) | 2 Jour. 
 
* Amélioration des messages de la gestion des erreurs et de la User Interface. | 1 Jour.
* Re-factoring du code. | 1/2 Jour.
* Streamer la liste des évènements. | 2 Jours. 