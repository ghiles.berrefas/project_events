#ifndef  SERVER_H
#define  SERVER_H 

#include "structure.h"

bool start_socket(int *fd_sock);
void add_event_to_buff(int8_t *buff, struct add_event_t *add_event);
void buff_to_list_event(struct list_events_t *list_event, int8_t *buff);
void buff_to_request_status(struct request_status_t *request_status, int8_t *buff);
bool get_input_admin(int8_t *buff);
void get_password(char *password);
bool get_input_client(int8_t *buff);
bool event_id_not_exists(int8_t id_event);
bool is_not_valid_date(char *date1) ;
bool get_input_registers_event(int8_t *buff);
void print_list_events();
void buff_to_clients_register(int8_t *line, int8_t *buff);

#endif 