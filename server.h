#ifndef  SERVER_H
#define  SERVER_H 

#include "structure.h"

bool start_listen_socket(int *fd_socket_server);
bool list_event_to_buff(int8_t *buff);
void request_status_to_buff(int8_t *buff, struct request_status_t request_status);
void buff_to_add_event(struct add_event_t *add_event, int8_t *buff);
bool update_file(struct list_events_t *list_events);
bool clients_register_to_buff(int8_t *buff);
bool register_event(int8_t *buff);
bool delete_event_from_tab(int8_t *buff);
bool add_event_to_tab(struct add_event_t *add_event);
bool check_file();

#endif 