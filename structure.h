#ifndef  STRUCTURE_H
#define  STRUCTURE_H

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>   
#include <termios.h>
#include <time.h>

#define HOST              "127.0.0.1"
#define PORT              8080
#define MAX_CLIENTS       1000
#define LIST_EVENTS       1 
#define ADD_EVENT         2   
#define DELETE_EVENT      3
#define REGISTER_EVENT    4
#define AUTH_ADMIN        5
#define REQUEST_STATUS    6
#define CLIENTS_REGISTER  7
#define BUF_SIZE          4162
#define LINE_SIZE         256
#define NAME_EVENT_SIZE   64
#define STATUS_MSG_SIZE   128
#define NB_EVENTS_MAX     64
#define ADMIN_PASSWORD    "admin"
#define EVENTS_DB_PATH    "db/events.db"
#define CLIENTS_DB_PATH   "db/clients.db"

// DATA 
struct event_t{
    int8_t id_event; 
    char name_event[NAME_EVENT_SIZE]; 
};

struct list_events_t{
    int8_t packet_type;  
    int8_t nb_events;
    struct event_t tab_events[NB_EVENTS_MAX]; 
};  

struct request_status_t{
    int8_t packet_type;
    char status_msg[STATUS_MSG_SIZE];
};


//client admin 
struct add_event_t{
    int8_t packet_type; 
    struct event_t event;
}; 

struct del_event_t{
    int8_t packet_type; 
    int8_t id_event;
};

//client visiteur 
struct register_event_t{
    int8_t packet_type; 
    int8_t id_event;
    char date[11]; 
    char name[20];
};

//TODO
struct info_packet_t{
    int8_t packet_type;
    int32_t packet_size;
};

char *displays[] = {
    "\n\
    1 - Admin\n\
    2 - Visitor\n\n\
choose 1 or 2:",
    "\
    1 - Add event\n\
    2 - Delete event\n\
    3 - List events\n\
    4 - Display clients registered for events\n\
    b - Go to back\n",
    "Here is the list of available events\n"
};

#endif 


    

