#include <termios.h>
#include <time.h>
#include "errhandlers.h"
#include "client.h"


struct list_events_t list_event; 
struct register_event_t register_event;
struct del_event_t del_event;
bool is_admin = false;
bool is_visitor = false;

bool start_socket(int *fd_sock){
    struct sockaddr_in sockaddr;
    if ((*fd_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1){
        syserr("start_socket: Failure of creation a socket:\n");
        return true;
    }
    sockaddr.sin_family = AF_INET; 
    sockaddr.sin_addr.s_addr =inet_addr(HOST);  
    sockaddr.sin_port = htons(PORT);  
    if(connect(*fd_sock, (struct sockaddr*) &sockaddr, sizeof(sockaddr)) == -1){
        syserr("start_socket: connect failed");
        return true;
    }
    return false;
}

void add_event_to_buff(int8_t *buff, struct add_event_t *add_event){

    buff[0] = add_event->packet_type; 
    buff[1] = add_event->event.id_event;
    for (int i = 0, j = 2 ; i < NAME_EVENT_SIZE && j < BUF_SIZE; i++, j++){
        buff[j] = add_event->event.name_event[i];
    }
}

void buff_to_list_event(struct list_events_t *list_event,int8_t *buff){
    list_event->packet_type = buff[0];
    list_event->nb_events = buff[1];
    int j = 2;
    for (int k = 0; k < buff[1]; k++){
         list_event->tab_events[k].id_event=buff[j];
         j++;
         for (int i = 0; i < NAME_EVENT_SIZE; i++, j++){
             list_event->tab_events[k].name_event[i]=buff[j] ;
         }
     }
}

void  buff_to_request_status(struct request_status_t *request_status,int8_t *buff){
    request_status->packet_type = buff[0];
    for (int i = 0; i < STATUS_MSG_SIZE; i++){
        request_status->status_msg[i]=buff[i+1] ;
    }
}

bool get_input_admin(int8_t *buff){
    char opt[2];
    opt[0] = (char)getchar();
    while (getchar() != '\n');
    struct add_event_t add_event;
    char buf[NAME_EVENT_SIZE];
    char o = opt[0];
    switch (o){
        case '1':
            memset(&add_event, '\0', sizeof (struct add_event_t));
            add_event.packet_type = ADD_EVENT;
            add_event.event.id_event = 0;
            printf("Enter the name of the event you wish to add:\n");
            if (fgets(buf, sizeof(buf),  stdin) == NULL){
                syserr("get_input_admin: fgets failed");
                return true;
            }
            buf[strlen(buf) - 1] = '\0';
            strcpy(add_event.event.name_event, buf);
            add_event_to_buff(buff, &add_event);
            break;
        case '2':
            del_event.packet_type = DELETE_EVENT;
            char buff_event_id[3];
            printf("Enter the event id you wish to delete:\n");
            if (fgets(buff_event_id, sizeof (buff_event_id),  stdin) == NULL){
                syserr("get_input_admin: fgets failed");
                return true;
            }
            sscanf(buff_event_id, "%hhd", &del_event.id_event);
            buff[0] = DELETE_EVENT;
            buff[1] = del_event.id_event;
            break;
        case '3':
            buff[0] = LIST_EVENTS;
        break;

        case '4':
            buff[0] = CLIENTS_REGISTER;
        break;

        case 'b':
            is_admin = false;
            buff[0] = -1;
        break;
            
        default:
            errmsgf("get_input_admin: Option %c not spported\n", o);
        break;
    }
    return false;
}

void get_password(char *password){
    static struct termios oldt, newt;
    int i = 0, c;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= (tcflag_t)~(ECHO);          
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    
    while ((c = getchar()) != '\n' && c != EOF && i < 6){
        password[i++] = (char)c;
    }
    password[i] = '\0';
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

bool get_input_client(int8_t *buff){
    is_admin = false;
    char opt[2];
    
    do {
        puts(displays[0]);
        opt[0] = (char)getchar();
        while (getchar() != '\n');
        opt[1]='\0';
    }while(strcmp(opt,"2") !=0 && strcmp(opt, "1") != 0);

    if(strcmp(opt,"1") == 0){
        char password[6];
        do{
            printf("Please enter your password:\n");
            get_password(password);
        }while(strcmp(password, ADMIN_PASSWORD) != 0);
        is_admin = true; 
    }else if(strcmp(opt,"2") == 0){
        buff[0] = LIST_EVENTS;
        char buff_name[20];
        printf("Please enter your username:\n");
        if (fgets(buff_name, sizeof(buff_name),  stdin) == NULL){
            syserr("get_input_client: fgets failed, retured NULL");
            return true;
        }
        buff_name[strlen(buff_name) - 1] = '\0';
        strcpy(register_event.name, buff_name);
    } 
    return false;
}

bool event_id_not_exists(int8_t id_event){
    for(int8_t i = 0; i < list_event.nb_events; i++){
        if(id_event == list_event.tab_events[i].id_event)
            return false;
    }
    errmsgf("id event selected not found\n");
    return true; 
}

bool is_not_valid_date(char *date1) {
    if (strlen(date1) != 10){
        errmsgf("Invalid format date the size is not correct. Usage: dd/mm/yyyy\n");
        return true;
    }
    char date[11] ; 
    strcpy(date,date1);
    const char s[2] = "/";
    int j=0,dd,mm,yyyy;
    char *token;
    token = strtok(date, s); 
    if(strlen(token) != 2) {
        errmsgf("Invalid format date. Usage: dd/mm/yyyy\n");
        return true;
    }
    dd = atoi(token);
    while( token != NULL ) {
        if(j==1) {
            if(strlen(token) != 2){
                errmsgf("Invalid format date. Usage: dd/mm/yyyy\n");
                return true;
            }
            mm = atoi(token);
        }else if(j==2) {
            if(strlen(token) != 4){
                errmsgf("Invalid format date. Usage: dd/mm/yyyy\n");
                return true;
             }
            yyyy = atoi(token);
        }
        token = strtok(NULL, s);
        j++;
    }
    time_t t = time(NULL);
    struct tm tm = *localtime(&t); 
    if( yyyy < tm.tm_year + 1900  ){
        errmsgf("Choose another date after today. Year is not valid: %d < %d\n",yyyy , tm.tm_year + 1900);
        return true;
    }else if( yyyy == tm.tm_year + 1900){
        if (mm < tm.tm_mon + 1){
            errmsgf("Choose another date after today. Month is not valid: %d < %d\n",mm ,tm.tm_mon + 1);
            return true;
        }else if ( mm ==  tm.tm_mon + 1){
            if(dd < tm.tm_mday){
                errmsgf("Choose another date after today. Day is invalid: %d < %d\n",dd ,tm.tm_mday);
                return true;
            }
        }
    }
    if(yyyy>=1900 && yyyy<=9999){
        if(mm>=1 && mm<=12){
            if((dd>=1 && dd<=31) && (mm==1 || mm==3 || mm==5 || mm==7 || mm==8 || mm==10 || mm==12))
                return false;
            else if((dd>=1 && dd<=30) && (mm==4 || mm==6 || mm==9 || mm==11))
                return false;
            else if((dd>=1 && dd<=28) && (mm==2))
                return false;
            else if(dd==29 && mm==2 && (yyyy%400==0 ||(yyyy%4==0 && yyyy%100!=0)))
                return false;
            else{
                errmsgf("Day is invalid.\n");
                return true;
            }
        }
        else{
            errmsgf("Month is not valid.\n");
            return true;
        }
    }
    errmsgf("Year is not valid.\n");
    return true;
}

bool get_input_registers_event(int8_t *buff){
    char buf[20];
    register_event.packet_type = REGISTER_EVENT;
    buff[0] = REGISTER_EVENT;
    do{
        printf("Select the event ID for which you wish to register or (b to go back)\n");
        if (fgets(buf, sizeof(buf),  stdin) == NULL){
            syserr("get_input_registers_event: fgets failed");
            return true;
        }
        sscanf(buf, "%hhd", &register_event.id_event);
        if(buf[0] == 'b' ) {
            memset(buff, '\0', BUF_SIZE);
            buff[0] = REGISTER_EVENT;
            is_visitor = false;
            return true;
        }
    }while(event_id_not_exists(register_event.id_event));
    do{
        printf("date (dd/mm/yyyy): \n");
        if (fgets(buf, sizeof (buf),  stdin) == NULL){
            syserr("get_input_registers_event: fgets failed");
            return true;
        }
        strncpy(register_event.date, buf, 10);
        register_event.date[10] = '\0';
    }while(is_not_valid_date(register_event.date));

    buff[1] = register_event.id_event;
    int j = 2;
    for (int i= 0; i<11; i++ , j++){
        buff[j] = register_event.date[i] ;  
    }
    for (int i= 0; i<20; i++ , j++){
        buff[j] = register_event.name[i];  
    }
    return false;
}

void print_list_events(){
    puts(displays[2]);
    for(int8_t i = 0; i < list_event.nb_events; i++){
        printf("\t%d - %s\n", list_event.tab_events[i].id_event,list_event.tab_events[i].name_event);
    }
}

void buff_to_clients_register(int8_t *line,int8_t *buff){
    memset(line,'\0',sizeof(*line));
    int j =0;
    for (int i=1; i<BUF_SIZE-1;i++){
        if(buff[i] != '\n'){
            line[j] = buff[i];
            j++;
        }else{
            line[j] = '\0';
            printf("%s\n",line);
            j=0;
        }
    }
}

int main(void){

    struct request_status_t request_status;
    int fd_sock;
    int8_t type_packet; 
    int8_t buff[BUF_SIZE];
    int8_t line[LINE_SIZE];
    size_t nb_write, nb_read; 
    
    if(start_socket(&fd_sock))
        exit(EXIT_FAILURE);

    FILE* fd_sock_stream = fdopen(fd_sock, "w+");
    if(fd_sock_stream == NULL){
        syserr("main: fdopen failed"); 
        exit(EXIT_FAILURE);
    }
    memset(buff, '\0', BUF_SIZE);
    memset(line, '\0', LINE_SIZE);
    while(true){
        if(!is_admin && !is_visitor){
            get_input_client(buff);
        }
        if(is_admin){
            printf("\n\t=== [ADMIN] ===\n\n");
            puts(displays[1]);
            if (get_input_admin(buff)) continue;
        }
        if(is_visitor){
            printf("\n\t=== [VISITOR] ===\n\n");
            if(get_input_registers_event(buff)) continue;
        }
        if((nb_write = fwrite(buff, BUF_SIZE, 1, fd_sock_stream)) != 1) {
            syserr("main: Failed to fwrite");
        }

        if( fflush(fd_sock_stream)){
            syserr("main: cannot flush the stream");
        } 
        if((nb_read = fread(buff, BUF_SIZE, 1, fd_sock_stream) ) != 1) {
            syserr("main: fread failed");
        }
        type_packet = buff[0];
        switch (type_packet) {
            case LIST_EVENTS:
                buff_to_list_event(&list_event,buff);
                print_list_events();
                if(!is_admin) is_visitor = true; 
                break;
            case CLIENTS_REGISTER:
                buff_to_clients_register(line,buff);
                break;
            case REQUEST_STATUS: 
                memset(&request_status, '\0', sizeof(struct request_status_t));
                printf("'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''\n");
                buff_to_request_status(&request_status,buff);
                printf("\t%s\n",request_status.status_msg); 
                printf("'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''\n");
                break;
        }

    }
    return EXIT_SUCCESS;
}


