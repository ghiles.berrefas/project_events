SRV=server
CLI=client
SRC=server.c stdmsghandlers.c
SRC_2=client.c stdmsghandlers.c
OBJ=$(SRC:.c=.o)
OBJ_2=$(SRC_2:.c=.o)
CC=gcc
CFLAGS=-W -Wall -Wconversion
.PHONY: clean default run
default: $(SRV) $(CLI) run

server.o:  errhandlers.h structure.h server.h
client.o:  errhandlers.h structure.h client.h
 
%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
 
$(SRV): $(OBJ)
	$(CC) -o $@ $^

$(CLI): $(OBJ_2)
	$(CC) -o $@ $^
 
run: $(SRV) $(CLI)
	./$(SRV)
clean:
	rm -rf $(SRV) $(CLI) $(OBJ) $(OBJ_2) *~
