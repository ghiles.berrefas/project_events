#include "errhandlers.h" 
#include "server.h"


bool start_listen_socket(int *fd_socket_server){
    struct sockaddr_in sockaddr_server;
    if ((*fd_socket_server = socket(AF_INET, SOCK_STREAM, 0)) == -1){
        errmsgf("start_listen_socket: Failure of creation a socket: %s\n", strerror(errno));
        return true;
    } 
    sockaddr_server.sin_family = AF_INET;                    
    sockaddr_server.sin_addr.s_addr = inet_addr(HOST); 
    sockaddr_server.sin_port = htons(PORT);                   
    if (bind(*fd_socket_server, (struct sockaddr *)&sockaddr_server, sizeof(sockaddr_server)) < 0){ 
        errmsgf("start_listen_socket: bind failded: %s\n", strerror(errno));
        return true;
    }
    if (listen(*fd_socket_server, MAX_CLIENTS) < 0){
        errmsgf("start_listen_socket: listen failded: %s\n", strerror(errno));
        return true;
    }
    return false;
}

bool list_event_to_buff(int8_t *buff){
    struct list_events_t list_events;  
    memset(&list_events, '\0', sizeof(struct list_events_t));
    FILE *fd_event_data;
    if ((fd_event_data = fopen(EVENTS_DB_PATH, "rb+")) == NULL){
        syserr("list_event_to_buff: fopen failed");
        return true;
    }
    size_t nb_read;
    if( (nb_read = fread(&list_events, sizeof(struct list_events_t), 1, fd_event_data)) != 1){
        memset(&list_events, '\0', sizeof(struct list_events_t));
        if (ferror(fd_event_data)) {
            syserr("list_event_to_buff: fread failed");
            return true;
        } 
        return true;
    }
    if (list_events.nb_events == 0) return true;
    buff[0] = LIST_EVENTS;
    buff[1] = list_events.nb_events;
    int j = 2;
    for (int k = 0; k < list_events.nb_events; k++){
        buff[j] = list_events.tab_events[k].id_event;
        j++;
        for (int i = 0; i < NAME_EVENT_SIZE; i++, j++){
            buff[j] = list_events.tab_events[k].name_event[i];
        }
    }
    return false;
}

void request_status_to_buff(int8_t *buff,struct request_status_t request_status){
    buff[0] = request_status.packet_type;
    for (int i = 0; i < STATUS_MSG_SIZE; i++){
        buff[i+1] = request_status.status_msg[i] ;
    }
}

void buff_to_add_event(struct add_event_t *add_event,int8_t *buff){
    add_event->packet_type = buff[0];
    add_event->event.id_event = buff[1];
    int j = 2; 
    for(int i =0; i<NAME_EVENT_SIZE; i++,j++){
        add_event->event.name_event[i] = buff[j];
    }
}   

bool update_file(struct list_events_t *list_events){
    FILE *fd_event_data;
    if ((fd_event_data = fopen(EVENTS_DB_PATH,"w+")) == NULL){
        syserr("update_file: fopen failed");
        return true;
    }
    if( fwrite(list_events, sizeof(struct list_events_t), 1, fd_event_data)!=1){
        syserr("update_file: fwrite failed");
        return true;
    }
    if (fclose(fd_event_data)){
        syserr("update_file: fclose failed");
        return true;
    }
    return false;
}

bool clients_register_to_buff(int8_t *buff){
    char line[LINE_SIZE] = {0};
    FILE *fd_clients_register;
    if ((fd_clients_register = fopen(CLIENTS_DB_PATH,"a+")) == NULL){
        syserr("clients_register_to_buff: fopen failed");
        return true;
    }
    buff[0] = CLIENTS_REGISTER;
    size_t j=1;
    while (fgets(line, LINE_SIZE, fd_clients_register)){
        for(size_t i = 0; i< strlen(line) ; i++){
            buff[j]= line[i];
            j++;
        }
        if (line[strlen(line) - 1] != '\n')
            printf("\n");
    }
    if (fclose(fd_clients_register)){
        syserr("clients_register_to_buff: fclose failed");
        return true;
    }
    return false;
}

bool register_event(int8_t *buff){
    struct register_event_t register_event; 
    register_event.packet_type = REGISTER_EVENT ; 
    register_event.id_event = buff[1];
    int j = 2;
    for (int i= 0; i<11; i++ , j++){
        register_event.date[i] = buff[j];  
    }
    for (int i= 0; i<20; i++ , j++){
        register_event.name[i] = buff[j];  
    }
    FILE *fd_clients_register;
    if ((fd_clients_register = fopen(CLIENTS_DB_PATH,"a+")) == NULL){
        syserr("register_event: fopen failed");
        return true;
    }
    if(strcmp(register_event.name, "") == 0){
        errmsgf("register_event: register_event.name = '%s' doesn't exist\n","");
        return true;
    }
    char result[LINE_SIZE];
    sprintf(result,"### %s is subsribe to event %hhd for %s\n", register_event.name,register_event.id_event,register_event.date);
    
    if (fwrite(result,strlen(result),1,fd_clients_register) != 1 ){
        syserr("register_event: fwrite failed"); 
        return true;
    }
    if (fclose(fd_clients_register)){
        syserr("register_event: fclose failed");
        return true;
    }
    return false;
}

bool delete_event_from_db(int8_t *buff){
    struct list_events_t list_events;  
    memset(&list_events, '\0', sizeof(struct list_events_t));
    FILE *fd_event_data;
    if((fd_event_data = fopen(EVENTS_DB_PATH,"r+")) == NULL){
        syserr("delete_event_from_tab: fopen failed");
        return true;
    }
    if(fread(&list_events, sizeof(struct list_events_t), 1, fd_event_data) != 1){
        syserr("delete_event_from_tab: fread failed");
        if (ferror(fd_event_data)) {
            return true;
        } 
    }
    struct list_events_t new_list_events;  
    memset(&new_list_events, '\0', sizeof(struct list_events_t));
    new_list_events.packet_type = list_events.packet_type;
    new_list_events.nb_events = list_events.nb_events;
    for(int8_t i = 0; i < list_events.nb_events; i++){
        if(buff[1] == list_events.tab_events[i].id_event){ 
            list_events.tab_events[i].id_event = 0; 
            for (int j = 0; j < NAME_EVENT_SIZE; j++){
                list_events.tab_events[i].name_event[j] = '\0' ;
            }
            new_list_events.nb_events = (int8_t)(list_events.nb_events-1);
        }
    }
    if( new_list_events.nb_events == list_events.nb_events) return true;
    int8_t k =0;
    for(int8_t i = 0; i < list_events.nb_events;i++){
        if(list_events.tab_events[i].id_event != 0){
            new_list_events.tab_events[k].id_event = (int8_t)(k+1); 
            for (int j = 0; j < NAME_EVENT_SIZE;j++){
                new_list_events.tab_events[k].name_event[j]=list_events.tab_events[i].name_event[j];
            }
            k++;
        }
    }
    update_file(&new_list_events);
    return false; 
}

bool add_event_to_tab(struct add_event_t *add_event){
    struct list_events_t list_events;  
    memset(&list_events, '\0', sizeof(struct list_events_t));
    FILE *fd_event_data;
    if ((fd_event_data = fopen(EVENTS_DB_PATH,"r+")) == NULL){
        syserr("add_event_to_tab: fopen failed");
        return true;
    }
    size_t nb_read;
    if( (nb_read = fread(&list_events, sizeof(struct list_events_t), 1, fd_event_data)) != 1){
        syserr("add_event_to_tab: fread failed");
        if (ferror(fd_event_data)) {
            return true;
        } 
    }

    if (list_events.nb_events >= NB_EVENTS_MAX){
        return true;
    }
    add_event->event.id_event = (int8_t)(list_events.tab_events[list_events.nb_events > 0 ? list_events.nb_events-1 : 0].id_event +1);
    list_events.tab_events[list_events.nb_events++] = add_event->event;
    if (fclose(fd_event_data)){
        syserr("add_event_to_tab: fclose failed");
        return true;
    }
    update_file(&list_events);
    return false;
}   

bool check_file(){
    if( access(EVENTS_DB_PATH, F_OK ) != 0 ) {
        FILE *fd_event_data;
        if ((fd_event_data = fopen(EVENTS_DB_PATH,"w+")) == NULL){
            syserr("check_file: fopen failed");
            return true;
        }
        if (fclose(fd_event_data)){
            syserr("check_file: fclose failed");
            return true;
        }
    }
    FILE * clients_register;
    if ((clients_register = fopen(CLIENTS_DB_PATH,"w+")) == NULL){
        syserr("check_file: fopen failed");
        return true;
    }
    if (fclose(clients_register)){
        syserr("check_file: fclose failed");
        return true;
    }
    return false;
}

int main(int argc, char *argv[]){
    printf("argc=%d, argv[0]=%s\n",argc, argv[0]);
    int fd_socket_server;
    struct sockaddr_in sockaddr_client;
    int8_t buff[BUF_SIZE];
    
    if (check_file())
        exit(EXIT_FAILURE);
    if (start_listen_socket(&fd_socket_server))
        exit(EXIT_FAILURE);

    printf("Server started on HOST: %s PORT: %d\nWaiting connections ... <<CTRL+C>> to exit\n", HOST, PORT);
    fd_set fds, readfds;
    FD_ZERO(&fds);                  
    FD_SET(fd_socket_server, &fds); 
    socklen_t len_addr_client = sizeof(struct sockaddr_in);
    int fd_max = fd_socket_server;
    int8_t type_packet;
    size_t nb_write, nb_read;
    struct request_status_t request_status;
    struct add_event_t add_event;
    FILE *stream_client[MAX_CLIENTS];
    for (;;){
        readfds = fds;
        if (select(fd_max + 1, &readfds, NULL, NULL, NULL) == -1){
            syserr("main: select failed");
        }
        for (int fd = 0; fd < (fd_max + 1) && fd < MAX_CLIENTS; fd++){
            if (FD_ISSET(fd, &readfds)){ 
                if (fd == fd_socket_server){ 
                    int fd_new_socket_client;
                    if ((fd_new_socket_client = accept(fd_socket_server, (struct sockaddr *)&sockaddr_client, &len_addr_client)) == -1) syserr("main: accept failed");
                    FD_SET(fd_new_socket_client, &fds);
                    stream_client[fd_new_socket_client] = fdopen(fd_new_socket_client, "w+");
                    if (fd_new_socket_client > fd_max) fd_max = fd_new_socket_client;
                    printf("Connection from %s:%d accepted fd: %d\n", inet_ntoa(sockaddr_client.sin_addr), ntohs(sockaddr_client.sin_port),fd_new_socket_client);
                }
                else{ 
                    if( (nb_read = fread(buff, BUF_SIZE, 1, stream_client[fd])) != 1 ){
                        syserr("main: fread failed");
                    }
                    if (nb_read == 0){
                        fprintf(stderr, "Socket %d closed by client\n", fd);
                        if (fclose(stream_client[fd]) != 0 ) syserr("main: fclose failed");
                        FD_CLR(fd, &fds);
                    }else{
                        type_packet = buff[0];
                        switch (type_packet) {
                            case LIST_EVENTS:
                                if(list_event_to_buff(buff)) {
                                    memset(&request_status, '\0', sizeof(struct request_status_t));
                                    request_status.packet_type = REQUEST_STATUS;
                                    sprintf(request_status.status_msg,"%s","A problem occurred when loading the events list.\n\tNo events are available, the administrator can add");
                                    request_status_to_buff(buff,request_status);
                                }

                                break;
                            case CLIENTS_REGISTER:
                                if(clients_register_to_buff(buff)){
                                    memset(&request_status, '\0', sizeof(struct request_status_t));
                                    request_status.packet_type = REQUEST_STATUS;
                                    sprintf(request_status.status_msg,"%s","A problem occurred when loading the clients registered for events");
                                    request_status_to_buff(buff,request_status);
                                }
                                break;
                            case REGISTER_EVENT:
                                memset(&request_status, '\0', sizeof(struct request_status_t));
                                request_status.packet_type = REQUEST_STATUS;
                                if(register_event(buff)){
                                    sprintf(request_status.status_msg,"%s","A problem occurred when registering for the event");
                                }else{
                                    sprintf(request_status.status_msg,"%s","You have successfully registered for the event");
                                }
                                request_status_to_buff(buff,request_status);
                                break;
                            case ADD_EVENT:
                                memset(&add_event, '\0', sizeof(struct add_event_t));
                                buff_to_add_event(&add_event, buff);
                                memset(&request_status, '\0', sizeof(struct request_status_t));
                                request_status.packet_type = REQUEST_STATUS;
                                if(add_event_to_tab(&add_event)){
                                    sprintf(request_status.status_msg,"%s","A problem occurred when adding the event");
                                }else{
                                    sprintf(request_status.status_msg,"%s%hhd","Event successfully added Id = ", add_event.event.id_event);
                                }
                                request_status_to_buff(buff,request_status);
                                break;
                            case DELETE_EVENT:
                                memset(&request_status, '\0', sizeof(struct request_status_t));
                                request_status.packet_type = REQUEST_STATUS;
                                if(delete_event_from_db(buff)){
                                    sprintf(request_status.status_msg, "%s", "A problem occurred when deleting event");
                                }
                                else{
                                    sprintf(request_status.status_msg, "%s%hhd", "Event successfully delete Id = ", buff[1]);
                                }
                                request_status_to_buff(buff, request_status);
                                break;
                        }   
                        if (( nb_write = fwrite(buff, BUF_SIZE, 1, stream_client[fd])) != 1 ){
                            syserr("fwrite failed");
                        }
                        fflush(stream_client[fd]);
                    }
                }
            }
        }
    }
    return EXIT_SUCCESS;
}
