# Projet de programmation réseau: Gestion et participation à des évènements (Clients - Server)

Ce projet est une application console dont le but est la gestion et la participation à des événements. Elle est architecturée selon le modèle Client - Server dont la communication est basée sur le protocole de la couche transport TCP; `socket` conforme à la norme `POSIX`. 

L'application a deux types de clients:
* Les clients `admin` ont accès à la liste des événements ainsi que la liste des clients inscrits aux événements et ont également la possibilité d'ajouter et de supprimer des événements. .

* Les clients `visitor` ont la possibilité de visualiser les événements disponibles publiés par les administrateurs. Ils peuvent s'inscrire à un événement en précisant une date.

# Compilation et lancement

Ouvrir au moins deux terminaux linux (un pour le `server`, et les autres pour les instances de `client`), et lancez la commande suivante sur le premier terminal:
```bash
$ make                
```
`make` compile les sources, génère les exécutables `server`, `client` et lance le `server`.

```bash
./server
argc=1, argv[0]=./server
Server started on HOST: 127.0.0.1 PORT: 8080
Waiting connections ... <<CTRL+C>> to exit
```
# Test
Dans une terminal exécutez la commande:
```bash
$ ./client             ​
```
Puis suivre le programme …
```bash
    1 - Admin
    2 - Visitor

choose 1 or 2:          ​
```
NB: Le mot de passe pour le rôle administrateur est `admin`