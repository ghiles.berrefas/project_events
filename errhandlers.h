#ifndef  ERR_HANDLERS_H
#define  ERR_HANDLERS_H
 
#include <stdio.h>

#ifdef DEBUG
#define debug_printf(fmt, ...) fprintf(stderr, fmt, __VA_ARGS__)
#else
#define debug_printf(fmt, ...)  do {} while(0)
#endif

void errmsgf(char *format, ...);
void syserr(char *message);

#endif